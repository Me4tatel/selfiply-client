import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';

import logo from '../../../view/img/logo.svg';
import arrowRight from '../../../view/img/icons/icon-arrow-right.svg';

import { myInput } from './input';
import { requiredInput, onlyNumber } from './validation';

class RegisterForm extends Component{
    render(){
        const {handleSubmit} = this.props;
        return(
            <form className="register-form" onSubmit={handleSubmit}>
                <div className="top">
                    <img src={logo} alt="Riddarens"/>
                </div>
                <div className="bottom">
                    <div className="field">
                        <Field
                            name="id"
                            component={myInput}
                            type="text"
                            placeholder="Your ID number"
                            validate={[requiredInput, onlyNumber]}
                        />
                    </div>
                    <button type="submit" className="btn submit-register">
                        Proceed
                        <img src={arrowRight} alt=""/>
                    </button>
                </div>
            </form>
        )
    }
}

RegisterForm = reduxForm ({
    form: 'login'
}) (RegisterForm);

export default RegisterForm