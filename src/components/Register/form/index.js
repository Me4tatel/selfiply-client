import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';

import logo from '../../../view/img/logo.svg';
import arrowRight from '../../../view/img/icons/icon-arrow-right.svg';

import { myInput } from './input';
import { requiredInput, onlyNumber } from './validation';
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import {setId} from "../../../transportData/actions/ids";

class RegisterForm extends Component{
    render(){
        const {handleSubmit} = this.props;
        return(
            <form className="register-form" onSubmit={handleSubmit}>
                <div className="top">
                    <img src={logo} alt="Riddarens"/>
                </div>
                <div className="bottom">
                    <div className="field">
                        <Field
                            name="id"
                            component={myInput}
                            type="text"
                            placeholder="Your ID number"
                            validate={[requiredInput, onlyNumber]}
                        />
                    </div>
                    <button type="submit" className={"btn submit-register" + (this.props.sendRegistarion === "LOGIN_LOADING"?' loading': '')}>
                        <span className="main">
                            Proceed
                            <img src={arrowRight} alt=""/>
                        </span>
                        <div className="spinner">
                            <div className="rect1"></div>
                            <div className="rect2"></div>
                            <div className="rect3"></div>
                            <div className="rect4"></div>
                            <div className="rect5"></div>
                        </div>
                    </button>
                </div>
            </form>
        )
    }
}

RegisterForm = reduxForm ({
    form: 'login'
}) (RegisterForm);

export default withRouter(connect(
    (state, ownProps) => ({
        sendRegistarion: state.state
    })
)(RegisterForm));