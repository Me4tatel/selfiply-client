export const requiredInput = (input) =>
    input && input.length === 12 ? undefined : `Please input id`;

export const onlyNumber = (input) =>
    !(/\D/.test(input)) ? undefined : `Please input only number`;