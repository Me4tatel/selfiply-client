import React, {Component, Fragment} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Scrollbar from 'react-scrollbars-custom';



import logo from '../../view/img/logo.svg';
import arrowRight from '../../view/img/icons/icon-arrow-right-green.svg';
import arrowTop from "../../view/img/icons/icon-arrow-top.svg";
import arrowBottom from "../../view/img/icons/icon-arrow-bottom.svg";

class Register extends Component{
    constructor(props){
        super(props);
        this.state = {
            scroll:{},
            scrollTop:0
        };
        this.handleScroll = this.handleScroll.bind(this);
    }

    handleScroll(props, lastProps){
        this.setState({
            scroll: props
        })
    }

    scrollTo(direction){
        console.log(direction);
        let { scroll } = this.state;
        if(direction === "top"){
            let scrollTop = scroll.scrollTop - 80 >= 0 ? scroll.scrollTop - 80 : 0;
            let different = scroll.scrollTop - scrollTop;
            if(different > 0){
                for(let i = 0; i<different; i+=3){
                    setTimeout(() => (this.setState({
                        scrollTop: scroll.scrollTop - i
                    })), i * 2);
                }
            }
        } else {
            let scrollTop = scroll.scrollTop + 80 + scroll.clientHeight <= scroll.scrollHeight  ? scroll.scrollTop + 80 : scroll.scrollHeight - scroll.clientHeight;
            let different = scrollTop - scroll.scrollTop;
            if(different > 0){
                for(let i = 0; i<different; i+=3){
                    setTimeout(() => (this.setState({
                        scrollTop: scroll.scrollTop + i
                    })), i);
                }
            }

        }

    }

    render(){
        return (
            <div className="page-choose">
                <header>
                    <div className="d-flex f-between f-align-center">
                        <div className="left">
                            <h2>Choose your concern</h2>
                        </div>
                        <div className="right logo">
                            <img src={logo} alt=""/>
                        </div>
                    </div>
                </header>
                <div className="content">
                    <Scrollbar
                        onScroll={this.handleScroll}
                        style={{width: '100%', height: '100%'}}
                        scrollTop={this.state.scrollTop}
                        trackYRenderer={
                           props => {
                               const {elementRef, ...restProps} = props;
                               return <span {...restProps}
                                            className="track"
                                            ref={elementRef}/>
                           }
                        }
                        thumbYRenderer={
                           props => {
                               const {elementRef, ...restProps} = props;
                               return(
                                   <Fragment>
                                       <div {...restProps} className="scroll-bar" ref={elementRef} />
                                       <div className="btn-to-top" onClick={()=>{this.scrollTo('top')}}>
                                           <img src={arrowTop} alt=""/>
                                       </div>
                                       <div className="btn-to-bottom" onClick={()=>{this.scrollTo('bottom')}}>
                                           <img src={arrowBottom} alt=""/>
                                       </div>
                                   </Fragment>
                               )
                           }
                        }>
                        <div className="concerns d-flex f-wrap">
                            {this.props.concerns && this.props.concerns.map((concern) => (
                                <div className="block block-3" key={concern.id}>
                                    <Link className="concern" to={"/concern/" + concern.id}>
                                        <p className="name">{concern.name}</p>
                                        <div className="right">
                                            <img src={arrowRight} alt=""/>
                                        </div>
                                    </Link>
                                </div>
                            ))}
                        </div>
                    </Scrollbar>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        concerns: state.concerns,
        ownProps
    })
)(Register));