const dataTrasporter = require('../core/data-trasporter.js').default;

export const setId = (id) =>{
    return dataTrasporter.get(`Patients?personalNumber=${id}`)
        .then(result=>{
            if(result.error){
                throw new Error("Error in downloading objects");
            }
            return {
                type: 'SET_ID',
                payload: result.data.data.personalNumber
            };
        })
        .catch(error=>{
            console.log(error);
        });
}