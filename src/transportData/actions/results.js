const dataTrasporter = require('../core/data-trasporter.js').default;

export const sendResult = (data) => {
    return dataTrasporter.post('Patients', data,
            {
                headers: {
                    "Content-Type": "application/json-patch+json"
                }
            }
        )
        .then(result=>{
            if(result.error){
                throw new Error("Error in downloading objects");
            }
            return {
                type: 'SEND_SUCCESS',
            };
        })
        .catch(error=>{
            console.log(error);
        });
};