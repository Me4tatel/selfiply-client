const GET_CONCERNS = "GET_CONCERNS";
const DOWLOAD_CONCERN_SYMPTOMS = "DOWLOAD_CONCERN_SYMPTOMS";

export default function (state = [], action){
	switch (action.type) {
		case GET_CONCERNS:{
            let newObj = state.concat( action.payload );
			return newObj;
		}

        case DOWLOAD_CONCERN_SYMPTOMS:{
            let newObj = state.map((item)=>{
                if(item.id === action.payload.id){
                    return{
                        ...item,
                        symptoms: action.payload.concernSymptoms
                    };
                }
                return item;
            });
            return newObj;
        }

		default:
			return state
	}
}
